# Contrôle optimal

Cours de contrôle optimal de l'ENSEEIHT pour le département Sciences du Numérique.

**Remarque préliminaire.** Voir la [documentation générale](https://gitlab.irit.fr/toc/etu-n7/documentation) pour récupérer le cours (clonage d'un projet Git), pour l'utilisation de Julia à l'N7, etc.

**CTD**

* [Notes de cours - contrôle optimal](https://gitlab.irit.fr/toc/etu-n7/controle-optimal/-/raw/master/notes-co-2024.pdf)

**TP**

Systèmes dynamiques

* [Sujet 1 : tp/ode.ipynb](tp/ode.ipynb)

Méthodes indirectes et directes en contrôle optimal

* [Sujet 2 : tp/simple-shooting.ipynb](tp/simple-shooting.ipynb) - Tir simple indirect
* [Sujet 3 : tp/direct-indirect.ipynb](tp/direct-indirect.ipynb) - Méthode directe et tir avec structure sur le contrôle

Calcul différentiel et équation différentielle ordinaire

* [Sujet 4 : tp/derivative.ipynb](tp/derivative.ipynb) - Calcul numérique de dérivées
* [Sujet 5 : tp/runge-kutta.ipynb](tp/runge-kutta.ipynb) - Méthodes de Runge-Kutta

Projet

* [Projet : tp/transfert-orbital.ipynb](tp/transfert-orbital.ipynb) - Transfert orbital

**Notes de cours supplémentaires - ressources**

* [Automatique](https://gitlab.irit.fr/toc/etu-n7/controle-optimal/-/raw/master/ressources/notes-autom-2021.pdf)
* [Calcul différentiel et équations différentielles ordinaires](https://gitlab.irit.fr/toc/etu-n7/controle-optimal/-/raw/master/ressources/notes-cd-edo-2024.pdf)
